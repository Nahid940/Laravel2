<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\mytest;

class myController extends Controller
{
	
	/**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
	
    public function create(){
		return view('test.myform');
	}
	
	public function about(){
		return view('about');
	}
	
	public function index(Request $request){
		$alldata=mytest::orderBy('name');
		$name=$request->input('name');
		if(!empty($name)){
			$alldata->Where('name','LIKE','%'.$name.'%');
		}
		$alldata=$alldata->paginate(5);
		return view('test.details',compact('alldata'));
	}
	
	public function store(Request $request)
	{
		$input = $request -> all();
		mytest::create($input);
		return redirect('test');
	}
	
	 public function show($id)
    {
		//echo $age->age;
		$name = mytest::table('mydata')->where($id,1);
		//$name = mytest::first()->name;
		//$name = mytest::all()->random(1);
		return view('test.myform',compact('name'));
		
		
        //return view('test.myform', ['user' => User::findOrFail($id)]);
    }
	
	
	public function update(Request $request,$id)
	{
		$input = $request -> all();
		$data=mytest::findOrFail($id);
		$data->update($input);
		return redirect('test');
	}
	

	
	public function edit($id)
	{
		$test=mytest::findOrFail($id);
		//return view('test.edit');
		return view('test.edit',compact('test'));
	}
	
	public function destroy($id){
		$data=mytest::findOrFail($id);
		$data->delete();
		return redirect('test');
	}
	
	
}
