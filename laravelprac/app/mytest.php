<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mytest extends Model
{
    //
	protected $table="mydata";
	protected $primaryKey="pracid";
	protected $fillable=['name','age','address','phone'];
}
