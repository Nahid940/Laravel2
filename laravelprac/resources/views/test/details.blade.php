<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		  <table class="table">
    <thead>
	{!!Form::open(['route'=>'test.index','method'=>'GET'])!!}
		{{Form::text('name',$value=null,$attributes = array('required','class'=>'form-control'))}}
			{{ Form::submit('Search', $attributes = array('class'=>'btn btn-success'))}}
		{!!Form::close()!!}
      <tr>
        <th>Sl.</th>
        <th>Name</th>
        <th>Age</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
	@foreach($alldata as $data)
      <tr>
			<td>{{$data -> pracid}}</td>
			<td>{{$data -> name}}</td>
			<td>{{$data -> age}}</td>
			<td>{{$data -> address}}</td>
			<td>{{$data -> phone}}</td>
			<td><a href="{{route('test.edit',$data->pracid)}}" class="btn btn-info">Edit</a>
			
			</td>
			<td>
				{!! Form::open(array('route' => ['test.destroy',$data->pracid], 'class'=>'form-horizontal','method'=>'delete')) !!}
				{!!Form::hidden('id',$data->pracid)!!}
					{!!Form::submit('Delete',['class'=>'btn btn-danger'])!!}
						{!!Form::close()!!}
			</td>
      </tr> 
	@endforeach
     
    </tbody>
  </table>
  {!! $alldata->render() !!}
	</div>
	
</body>
</html>