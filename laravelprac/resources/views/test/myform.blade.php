<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Form</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="col-sm-12">
	<div class="jumbotron">
	<div class="page-header">
		<center><h1>This form is running by laravel</h1></center>
	</div>
	
	</div>
	
	 {!! Form::open(array('route' => 'test.store', 'class'=>'form-horizontal')) !!}
	 
		<div class="form-group">
		  {{Form::label('name', 'Name : ',$attribute=array('class'=>'control-label col-sm-2'))}}
		  <div class="col-sm-4">
				{{Form::text('name',$value=null,$attributes = array('required','class'=>'form-control'))}}
		  </div>
		  {{Form::label('age', 'Age : ',$attribute=array('class'=>'control-label col-sm-2'))}}
		   <div class="col-sm-4">
				{{Form::text('age',$value=null,$attributes = array('required','class'=>'form-control'))}}
		   </div>
		</div>
		
		<div class="form-group">
			 {{Form::label('address', 'Address : ',$attribute=array('class'=>'control-label col-sm-2'))}}
		   <div class="col-sm-4">
				{{Form::text('address',$value=null,$attributes = array('required','class'=>'form-control'))}}
		   </div>
		   
		   
		    {{Form::label('phone', 'Phone : ',$attribute=array('class'=>'control-label col-sm-2'))}}
		   <div class="col-sm-4">
				{{Form::text('phone',$value=null,$attributes = array('required','class'=>'form-control'))}}
		   </div>
		</div>
		
		<div class="form-group">
			
		</div>
		
	<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
			{{ Form::submit('Submit', $attributes = array('class'=>'btn btn-success'))}}
      </div>
    </div>
			
		{!! Form::close() !!}
		
		@if (date('Y-m-d')=='2017-06-12')
			Today!
		@else
			Not today!
		@endif
		
		{{$name->name }}
			
		
	</div>
</body>
</html>