<?php


class UsersTableSeeder extends \Illuminate\Database\Seeder{
    public function run(){
        \Illuminate\Support\Facades\DB::table('users')->truncate();
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name'=>"Nahid",
            "email"=>"nahid942@gmail.com",
            "password"=>"123456"
        ]);
        $this->call('UsersTableSeeder');
    }
}