<?php
    $servername = "localhost";
    $user = "root";
    $password = "";
    $dbname = "youtube_project";
    $conn = new mysqli($servername, $user, $password, $dbname);
?>

   

   <?php
    session_start();
    if(isset($_POST['login'])){
        //require 'connection.php';
        $username=mysqli_real_escape_string( $conn,$_POST['username']);
        $password=mysqli_real_escape_string( $conn,$_POST['password']);
        $result=mysqli_query($conn,'select * from admin_login where username="'.$username.'" and password="'.$password.'"');
        if(mysqli_num_rows($result)==1){
            $_SESSION['username']=$username;
            header('Location:home.php');
            
        }else {
            echo "Invalid";
        }
    }

    if(isset($_POST['signup'])){
        header("Location:signup.php");
    }

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Bootstrap Login Form</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	
	
	<body>
<!--login modal-->
<form action="" method="post">
   
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">Login</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
            <div class="form-group">
              <input type="text" class="form-control input-lg" placeholder="Username" name="username">
            </div>
            <div class="form-group">
              <input type="password" class="form-control input-lg" placeholder="Password" name="password">
            </div>
            <div class="form-group">
              <button type="submit" name="login" class="btn btn-primary btn-lg btn-block">Log In</button>
              <span class="pull-right"><a href="#">Register</a></span><span><a href="#">Need help?</a></span>
            </div>
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		  </div>	
      </div>
  </div>
  </div>
</div>

</form>

	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>





