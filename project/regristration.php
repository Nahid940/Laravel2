

<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
		
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
      

<style>

</style>


</head>
<body>

    <div class="template">
			<div class="headseaction clear">
				<div class="headerleft clear">
					<h2>My template</h2>
					<p>Test template</p>
				</div>
				
				<div class="mainmenu">
				
				<div class="topbar">
				    <ul>
				        <li>
				    <?php
                    session_start();
                    if($_SESSION["username"]) {
                    ?>
                            Welcome <?php echo $_SESSION["username"]; ?>. Click here to <a href="logout.php" tite="Logout">Logout</a>
                    <?php
                    }
                    ?>
				        </li>
				    </ul>
				</div>
				    
					<ul>
						<li><a href="homepage.php">Home</a></li>
						<li><a id="active" href="">Menu 2</a></li>
						<li><a href="">Menu 3 <i class='fa fa-angle-down'></i></a>
							 <ul>
							  <li><a href="#">Category 1</a></li>
							  <li><a href="#">Category 2</a></li>
							</ul>
						</li>
						<li><a href="registration.php">Regristration</a></li>
						
					</ul>
				</div>
			</div>
			
			<div class="mainbody">
            <h2>Regrister here to be a member</h2>
            
			 <form action="" method="post" class="form-horizontal" >
			<div class="form-group">
				<label for="address" class="control-label col-md-2">First name :</label>
				
				<div class="col-md-4">
				<input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
				</div>
				
				
				<label for="address" class="control-label col-md-1">Email :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="address" class="control-label col-md-2">Address :</label>
				<div class="col-md-4"> 
				<input type="text" class="form-control" id="address"  name="address" placeholder="Enter address">
				</div>
				
				<label for="" class="control-label col-md-1">Age :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" id="age" name="age" placeholder="Enter age">
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Phn No. :</label>
				<div class="col-md-4">
				<input type="text" class="form-control" id="phn" name="phn" placeholder="Enter Phn. no.">
				</div>
			</div>
			
			<div class="form-group">
				<label for="Gender" class="control-label col-md-2">Gender :</label>
				<div class="col-md-4">
				<label class="radio-inline" > <input type="radio" name="optradio" value="male"/>Male</label>
				<label class="radio-inline" > <input type="radio" name="optradio" value="female"/>Female</label>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">DOB :</label>
				<div class="col-md-4">
				 <input type="date" name="dob"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Tour :</label>
				<div class="col-md-4">
				 <input type="date" name="dot"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Return :</label>
				<div class="col-md-4">
				 <input type="date" name="dor"/>
				</div>
			</div>
			
			
			<div class="col-sm-offset-6">
				<input type="submit" value="Submit" name="submit"/>
			</div>
			
			<div class="col-sm-offset-6">
				<input type="text" value="<?php echo (isset($dbh))?$dbh:'';?>"/>
			</div>
			
		</form>
			</div>
			
		</div>

       <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

	<script>
		$('.mainmenu ul li').hover(
		  function() {
			  $('ul', this).stop().slideDown(200);
		  },
			function() {
			$('ul', this).stop().slideUp(200);
		  }
		);
		</script>
</body>
</html>