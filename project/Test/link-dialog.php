<br><br><br><br>
<ol>
<li><a href=dialog-box1.php>Dialog Box: Display on load </a></li>
<li><a href=dialog-box1a.php>Dialog Box: Display after time delay </a></li>
<li><a href=dialog-box2.php>Dialog Box: Open on Button Click</a></li>
<li><a href=dialog-box3.php>Dialog Box: Add one Close  Button</a></li>
<li><a href=dialog-box4.php>Dialog Box: With Input box & Submit Button</a></li>
<li><a href=dialog-box5.php>Dialog Box: With Hide & show animation effects</a></li>
<li><a href=dialog-box6.php>Dialog Box: Passing data to Dialog box</a></li>
<li><a href=dialog-box7.php>Dialog Box: Displaying database record in Dialog box ( by entering ID)</a></li>
<li><a href=dialog-box8.php>Dialog Box: Displaying database record in Dialog box ( By button click)</a></li>
<li><a href=dialog-box9.php>Dialog Box: Displaying Form for user input and inserting data to table</a></li>
<li><a href=dialog-box10.php>Dialog Box: Edit / update  a record by usign a form in Dialog box</a></li>
</ol>