<!doctype html public "-//w3c//dtd html 3.2//en">
<html>
<head>
<title>Demo of displaying all columns of the record in a Dialog box after collecting record ID from user using JQuery</title>
<META NAME="DESCRIPTION" CONTENT="DEMO of showing full record details after entery of record ID by user using JQuery ">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="style.css">

</head>
<body>
Enter ID 1 to 37 <input type=text size=3 id=t1><br><button id="b1">Show record</button><div id=msg></div>
 <div id="my_dialog" title="plus2net  dialog">
  <div id=body></div>
</div>

<script>
   $(document).ready(function() {

$(function() {
    $( "#my_dialog" ).dialog({
		autoOpen: false,
buttons: {
        "Close ": function() {
          $( this ).dialog( "close" );
        }
      }

							});
});

$("#b1").click(function(){
$( "#my_dialog" ).dialog( "open" );
$('#body').text('Wait..');
var id=$('#t1').val();
$('#body').load("data.php?id="+id);
})

})
</script>

<?Php
//require "link-dialog.php";
?>
</body>
</html>
