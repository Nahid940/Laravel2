<html>
<head>
<title>Enable Disable Submit Button Based on Validation</title>
<style>
body{width:50%;min-width:200px;font-family:arial;}
#frmDemo {background: #98E6DB;padding: 40px;overflow:auto;}
#btn-submit{padding: 10px 20px;background: #555;border: 0;color: #FFF;display:inline-block;margin-top:20px;cursor: pointer;font-size: medium;}
#btn-submit:focus{outline:none;}
.input-control{padding:10px;width:100%;}
.input-group{margin-top:10px;}
#error_message{
    background: #F3A6A6;
}
#success_message{
    background: #CCF5CC;
}
.ajax_response {
    padding: 10px 20px;
    border: 0;
    display: inline-block;
    margin-top: 20px;
    cursor: pointer;
	display:none;
	color:#555;
}
</style>
</head>
<body>
<h1>jQuery Fade Out Message after Form Submit</h1>			
<form id="frmDemo" method="post">
   <div class="input-group">Name </div>
   <div>
        <input type="text" name="name" id="name" class="input-control" />
   </div>
    
   <div class="input-group">Message </div>
   <div>
		<textarea name="comment" id="comment" class="input-control"></textarea>
   </div>

   <div style="float:left">
        <button type="submit" name="btn-submit" id="btn-submit">Submit</button>
    </div>
	<div id="error_message" class="ajax_response" style="float:left"></div>
	<div id="success_message" class="ajax_response" style="float:left"></div>
</form>	
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>			
<script>
$("#frmDemo").submit(function(e) {
	e.preventDefault();
	var name = $("#name").val();
	var comment = $("#comment").val();

		$("#error_message").html("").hide();
		$.ajax({
			type: "POST",
			url: "post-form.php",
			data: "name="+name+"&comment="+comment,
			success: function(data){
				$('#success_message').fadeIn().html(data);
				setTimeout(function() {
					$('#success_message').fadeOut("slow");
				}, 1000 );
			}
		});
	//}
})
</script>	
</body>
</html>