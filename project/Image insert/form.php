<!DOCTYPE html>
<?php
include "Database.php";
include "config.php";
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>

<?php
 $db=new Database();
if(isset($_POST['submit'])){
	
	$name=mysqli_real_escape_string($db->link, $_POST['name']);
	$address=mysqli_real_escape_string($db->link,$_POST['address']);
	$phn=mysqli_real_escape_string($db->link,$_POST['phn']);
	$gender=mysqli_real_escape_string($db->link,$_POST['optradio']);
	$dob=mysqli_real_escape_string($db->link,$_POST['dob']);
	$email=mysqli_real_escape_string($db->link, $_POST['email']);
	$age=mysqli_real_escape_string($db->link,$_POST['age']);
	
	$dot=mysqli_real_escape_string($db->link,$_POST['dot']);
	$dor=mysqli_real_escape_string($db->link,$_POST['dor']);
	
	//select datediff('2010-04-15', '2010-04-12');
	
	$myQuery="Insert into person(name,address,phn,gender,dob,email,age,dot,dor) values('$name','$address','$phn','$gender','$dob','$email','$age','$dot','$dor')";
	
	
	
	$create=$db->insert($myQuery);
	if($create){
				echo "Done!!";
				}else {
					echo " Not inserted";
				}
}
?>

<body>
   <div class="main">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1-1</a></li>
            <li><a href="#">Page 1-2</a></li>
            <li><a href="#">Page 1-3</a></li>
          </ul>
        </li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav> 
       
   </div>
    
    <div class="leftbar">
        <div class="sidebar">
            <ul>
                <li><a href="values-of-butterfly.html">Values Of Butterfly</a></li>
                <li><a href="anatomy.html">Anatomy Of Butterfly</a></li>
            </ul>
            <input name="search" type="text" class="txt"/>
            <input name="search-btn" type="submit" class="btn" value="SEARCH"/>
            <img src="images/Search_logo.png" alt="Search_logo" class="sidebar_img"/> 
            
        </div>
    </div>
   
   
    <div class="contain">
               
    <div class="form col-md-10">
		<form action="" method="post" class="form-horizontal" >
			<div class="form-group">
				<label for="address" class="control-label col-md-2">First name :</label>
				
				<div class="col-md-4">
				<input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
				</div>
				
				
				<label for="address" class="control-label col-md-1">Email :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="address" class="control-label col-md-2">Address :</label>
				<div class="col-md-4"> 
				<input type="text" class="form-control" id="address"  name="address" placeholder="Enter address">
				</div>
				
				<label for="" class="control-label col-md-1">Age :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" id="age" name="age" placeholder="Enter age">
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Phn No. :</label>
				<div class="col-md-4">
				<input type="text" class="form-control" id="phn" name="phn" placeholder="Enter Phn. no.">
				</div>
			</div>
			
			<div class="form-group">
				<label for="Gender" class="control-label col-md-2">Gender :</label>
				<div class="col-md-4">
				<label class="radio-inline" > <input type="radio" name="optradio" value="male"/>Male</label>
				<label class="radio-inline" > <input type="radio" name="optradio" value="female"/>Female</label>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">DOB :</label>
				<div class="col-md-4">
				 <input type="date" name="dob"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Tour :</label>
				<div class="col-md-4">
				 <input type="date" name="dot"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Return :</label>
				<div class="col-md-4">
				 <input type="date" name="dor"/>
				</div>
			</div>
			
			
			<div class="col-sm-offset-6">
				<input type="submit" value="Submit" name="submit"/>
			</div>
			
			<div class="col-sm-offset-6">
				<input type="text" value="<?php echo (isset($dbh))?$dbh:'';?>"/>
			</div>
			
		</form>
  </div>
    </div>


</body>
</html>