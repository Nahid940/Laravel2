<?php
	$con=mysqli_connect("localhost","root","");
	$query="drop database if exists test1012";
	if(mysqli_query($con,$query)){
		echo "Database Dropped </br>";
	} 
	
	$query="create database test1012";
	mysqli_query($con,$query);
	$con=mysqli_connect("localhost","root","","test1012");
	
	$table1="CREATE TABLE customerreg (
	  customerno int(10) NOT NULL AUTO_INCREMENT,
	  name varchar(100) NOT NULL,
	  email varchar(100) NOT NULL,
	  address varchar(400) NOT NULL,
	  postcode varchar(400) NOT NULL,
	  contact varchar(100) NOT NULL,
	  password varchar(100) NOT NULL,
	  PRIMARY KEY (customerno))";
	if(mysqli_query($con,$table1)) echo "customerreg Table Created </br>";
	else echo mysqli_error($con);
	
	
	$table2="CREATE TABLE service (
  serviceid int(5) NOT NULL,
  service_name varchar(50) NOT NULL,
  PRIMARY KEY (serviceid))";
  
  if(mysqli_query($con,$table2)) echo "service Table Created </br>";
	else echo mysqli_error($con);
	
	
	$query="CREATE TABLE pet (
  petid int(5) NOT NULL,
  petname varchar(50) NOT NULL,
  pet_DOB varchar(10) NOT NULL,
  PRIMARY KEY (petid))";
  if(mysqli_query($con,$query)) echo "pet Table Created </br>";
	else echo mysqli_error($con);
	
	
	$query="CREATE TABLE servicepackage (
  packageid int(5) NOT NULL,
  serviceid int(5) NOT NULL,
  price float NOT NULL,
  duration varchar(50) NOT NULL,
  PRIMARY KEY (packageid),
  FOREIGN KEY (serviceid) REFERENCES service (serviceid))";
  
	if(mysqli_query($con,$query)) echo "servicepackage Table Created </br>";
	else echo mysqli_error($con);
	
	
	$query="CREATE TABLE customerpet (
  petid int(5) NOT NULL,
  customerno int(10) NOT NULL,
  PRIMARY KEY (petid,customerno),
  FOREIGN KEY (petid) REFERENCES pet(petid),
   FOREIGN KEY (customerno) REFERENCES customerreg (customerno))";
   if(mysqli_query($con,$query)) echo " customerpet Table Created </br>";
	else echo mysqli_error($con);
	
	
	$query="CREATE TABLE customerorder(
  orderid int(5) NOT NULL AUTO_INCREMENT,
  customerno int(10)  NOT NULL,
  packageid int(5)  NOT NULL,
  date varchar(20)  NOT NULL,
  servicetime varchar(20)  NOT NULL,
  petid int(5)  NOT NULL,
  PRIMARY KEY (orderid),
  FOREIGN KEY (customerno) REFERENCES customerreg (customerno),
  FOREIGN KEY (packageid) REFERENCES servicepackage (packageid),
  FOREIGN KEY (petid) REFERENCES pet (petid))";
  
    if(mysqli_query($con,$query)) echo " customerpet Table Created </br>";
	else echo mysqli_error($con);
	
	$query="CREATE TABLE previouslist(
  updateno int(5) NOT NULL AUTO_INCREMENT,
  customerno int(10) NOT NULL,
  packageid int(5) NOT NULL,
  date varchar(20) NOT NULL,
  servicetime varchar(25) NOT NULL,
  petid int(5) NOT NULL,
  PRIMARY KEY (updateno),

  FOREIGN KEY (customerno) REFERENCES customerreg (customerno),
  FOREIGN KEY (packageid) REFERENCES servicepackage (packageid),
   FOREIGN KEY (petid) REFERENCES pet (petid))";
  if(mysqli_query($con,$query)) echo " previouslist Table Created </br>";
	else echo mysqli_error($con);
	
	$query="CREATE TABLE LPS(
		id int (2) NOT NULL,
		postcode varchar(20) not null,
		PRIMARY KEY(id))";
	if(mysqli_query($con,$query)) echo " previouslist Table Created </br>";
	else echo mysqli_error($con);

?>