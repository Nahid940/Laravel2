<!DOCTYPE html>
<?php
include "Database.php";
include "config.php";
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>

<?php
 $db=new Database();
if(isset($_POST['submit'])){
	
	$name=mysqli_real_escape_string($db->link, $_POST['name']);
	$address=mysqli_real_escape_string($db->link,$_POST['address']);
	$phn=mysqli_real_escape_string($db->link,$_POST['phn']);
	$gender=mysqli_real_escape_string($db->link,$_POST['optradio']);
	$dob=mysqli_real_escape_string($db->link,$_POST['dob']);
	$email=mysqli_real_escape_string($db->link, $_POST['email']);
	$age=mysqli_real_escape_string($db->link,$_POST['age']);
	
	$dot=mysqli_real_escape_string($db->link,$_POST['dot']);
	$dor=mysqli_real_escape_string($db->link,$_POST['dor']);
	
	//select datediff('2010-04-15', '2010-04-12');
	
	$myQuery="Insert into person(name,address,phn,gender,dob,email,age,dot,dor) values('$name','$address','$phn','$gender','$dob','$email','$age','$dot','$dor')";
	
	
	
	$create=$db->insert($myQuery);
	if($create){
				echo "Done!!";
				}else {
					echo " Not inserted";
				}
}
?>

<body>

<div class="container">
  <div class="form col-md-10">
		<form action="" method="post" class="form-horizontal" >
			<div class="form-group">
				<label for="address" class="control-label col-lg-2">First name :</label>
				
				<div class="col-md-4">
				<input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
				</div>
				
				
				<label for="address" class="control-label col-md-1">Email :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="address" class="control-label col-md-2">Address :</label>
				<div class="col-md-4"> 
				<input type="text" class="form-control" id="address"  name="address" placeholder="Enter address">
				</div>
				
				<label for="" class="control-label col-md-1">Age :</label>
				<div class="col-md-3">
				<input type="text" class="form-control" id="age" name="age" placeholder="Enter age">
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Phn No. :</label>
				<div class="col-md-4">
				<input type="text" class="form-control" id="phn" name="phn" placeholder="Enter Phn. no.">
				</div>
			</div>
			
			<div class="form-group">
				<label for="Gender" class="control-label col-md-2">Gender :</label>
				<div class="col-md-4">
				<label class="radio-inline" > <input type="radio" name="optradio" value="male"/>Male</label>
				<label class="radio-inline" > <input type="radio" name="optradio" value="female"/>Female</label>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">DOB :</label>
				<div class="col-md-4">
				 <input type="date" name="dob"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Tour :</label>
				<div class="col-md-4">
				 <input type="date" name="dot"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="" class="control-label col-md-2">Date of Return :</label>
				<div class="col-md-4">
				 <input type="date" name="dor"/>
				</div>
			</div>
			
			
			<div class="col-sm-offset-6">
				<input type="submit" value="Submit" name="submit"/>
			</div>
			
			<div class="col-sm-offset-6">
				<input type="text" value="<?php echo (isset($dbh))?$dbh:'';?>"/>
			</div>
			
		</form>
	
  </div>
</div>

</body>
</html>