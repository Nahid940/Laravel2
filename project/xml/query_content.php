<?php
	header('Content-Type: text/xml;charset=utf-8');
	$host="localhost"; $user="root"; $pass=""; $database="dw";
	$con=mysqli_connect($host,$user,$pass,$database);
	$query="SELECT * from ingredient";
	$ret=mysqli_query($con,$query);
	$num_results=mysqli_num_rows($ret);
	$doc=new DOMDocument();
	$doc->formatOutput=true;
	$root=$doc->createElement("All_ingredient");
	$doc->appendChild($root);
	
	for ($i=0; $i<$num_results; $i++){
		$row=mysqli_fetch_array($ret, MYSQLI_ASSOC);
		$node=$doc->createElement("ingredients");
		
		$i_id=$doc->createElement("id");
		$i_name=$doc->createElement("name");
		
		$i_id->appendChild($doc->createTextNode($row["i_id"]));
		$i_name->appendChild($doc->createTextNode($row["i_name"]));
		
	$node->appendChild($i_id);
	$node->appendChild($i_name);
	$root->appendChild($node);
	}
	echo $doc->saveXML();
?>