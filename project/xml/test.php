<?php
	header('Content-Type: text/xml;charset=utf-8');
	$host="localhost"; $user="root"; $pass=""; $database="dw";
	$connection=mysqli_connect($host,$user,$pass,$database);
	$query="SELEST * FROM ingredient";
	$ret=mysqli_query($connection,$query);
	if($ret){
		$num_result=mysqli_num_rows($ret);
	}
	
	$doc=new DOMDocument();
	$doc->formatOutput=true;
	
	$root=$doc->createElement("all ingredents");
	for($i=0;$i<$num_result;$i++){
		$row=mysqli_fetch_array($ret,MYSQL_ASSOC);
		$node=$doc->createElement("ingerdient");
		
		$i_id=$doc->createElement("i_id");
		$i_name=$doc->createElement("i_name");
		
		$i_id->appendChild($doc->createTextNode($row["i_id"]));
		$i_name->appendChild($doc->createTextNode($row["i_name"]));
		
		$node->appendChild($i_id);
		$node->appendChild($i_name);
		$root->appendChild($node);
	}
	
	$doc->appendChild($root);
	//mysqli_close($connection);
	echo $doc->saveXML();
	
?>