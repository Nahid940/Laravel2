<?php
header('Content-Type: text/xml; charset=utf-8');
$host= "localhost"; $user = "root"; $pass = ""; $database= "event";
$con = mysqli_connect($host,$user,$pass,$database);
$query = "SELECT * from event";
$ret= mysqli_query($con,$query);
$num_results= mysqli_num_rows($ret);
$doc= new DOMDocument();
$doc->formateOutput=true;
$root= $doc->createElement("Event_Details");
$doc->appendChild($root);
for($i=0;$i<$num_results;$i++){
	$row= mysqli_fetch_array($ret,MYSQLI_ASSOC);
	$node= $doc->createElement("Event");
	$ID=$doc->createElement("ID");
	$Event_Title=$doc->createElement("Event_Title");
	$Event_Date=$doc->createElement("Event_Date");
	$Event_Time=$doc->createElement("Event_Time");
	$Event_Location=$doc->createElement("Event_Location");
	$Event_Description=$doc->createElement("Event_Description");
	
	
	$ID->appendChild($doc->createTextNode($row["eid"]));
	$Event_Title->appendChild($doc->createTextNode($row["etitle"]));
	$Event_Date->appendChild($doc->createTextNode($row["date"]));
	$Event_Time->appendChild($doc->createTextNode($row["time"]));
	$Event_Location->appendChild($doc->createTextNode($row["loc"]));
	$Event_Description->appendChild($doc->createTextNode($row["desc"]));
	$node->appendChild($ID);
	$node->appendChild($Event_Title);
	$node->appendChild($Event_Date);
	$node->appendChild($Event_Time);
	$node->appendChild($Event_Location);
	$node->appendChild($Event_Description);
	$root->appendChild($node);
}
echo $doc->saveXML();

?>