<?php
header('Content-Type: text/xml; charset=utf-8');
$host= "localhost"; $user = "root"; $pass = ""; $database= "l5dc";
$con = mysqli_connect($host,$user,$pass,$database);
$query = "SELECT * from ingredient";
$ret= mysqli_query($con,$query);
$num_results= mysqli_num_rows($ret);
$doc= new DOMDocument();
$doc->formateOutput=true;
$root= $doc->createElement("all_ingredients");
$doc->appendChild($root);
for($i=0;$i<$num_results;$i++){
	$row= mysqli_fetch_array($ret,MYSQLI_ASSOC);
	$node= $doc->createElement("Ingredient");
	$ID=$doc->createElement("ID");
	$name=$doc->createElement("Name");
	
	
	
	$ID->appendChild($doc->createTextNode($row["ID"]));
	$name->appendChild($doc->createTextNode($row["Name"]));
	
	$node->appendChild($ID);
	$node->appendChild($name);
	
	$root->appendChild($node);
}
echo $doc->saveXML();

?>