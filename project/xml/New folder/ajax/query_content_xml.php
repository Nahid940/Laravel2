<?php
header('Content-Type: text/xml; charset=utf-8');
$host= "localhost"; $user = "root"; $pass = ""; $database= "ajaxdb";
$con = mysqli_connect($host,$user,$pass,$database);
$query = "SELECT * from thing";
$ret= mysqli_query($con,$query);
$num_results= mysqli_num_rows($ret);
$doc= new DOMDocument();
$doc->formateOutput=true;
$root= $doc->createElement("all_things");
$doc->appendChild($root);
for($i=0;$i<$num_results;$i++){
	$row= mysqli_fetch_array($ret,MYSQLI_ASSOC);
	$node= $doc->createElement("thing");
	$ID=$doc->createElement("ID");
	$Description=$doc->createElement("Description");
	$ID->appendChild($doc->createTextNode($row["ID"]));
	$Description->appendChild($doc->createTextNode($row["Description"]));
	$node->appendChild($ID);
	$node->appendChild($Description);
	$root->appendChild($node);
}
echo $doc->saveXML();

?>