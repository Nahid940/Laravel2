<form action="" method="post" enctype="multipart/form-data"  id="regform" name="regform">
<input type="hidden" name="recordid" id="recordid" value="<?php echo $row['recordid'];?>"/>

<div id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <h3 class="text-center">Sign up</h3>
          <h5><span class="label label-success" id="result"></span></h5>
          <span  id="login"><a href="login.html">Log in</a></span>
      </div>
      
      <div class="modal-body">
<!--            <div class="col-lg-12">-->
           
           <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="">Name</label>
            <span class="must"> *</span>
            <span class="label label-danger" id="name"></span>
            <input class="form-control " type="text" name="name" placeholder="personname" id='personname'/>
            </div>
            
            </div>
            
            <div class='col-lg-6'>
            <div class="form-group">
            <label for="">ID</label>
            <span class="must"> *</span>
            <span class="label label-danger" id="idc"></span>
            <input type="text" class="form-control " placeholder="ID" name="id" id="id"/>
            </div>

       
            </div>
            
            </div>
            
<!--            ..........................-->
            
            <div class="row">
          
            
<!--            </div>-->
       
            
<!--        <div class="col-lg-12">-->
<!--        <div class="row">-->
            <div class='col-lg-6'>
            <div class="form-group">
            <label for="">Contact no.</label>
            <span class="must"> *</span>
            <span class="label label-danger" id="contactc"></span>
                 <input type="text" class="form-control" id="contact" placeholder="Contact No.*" name="contact"/>
            </div>
            </div>
            
            <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Address</label>
                      <span class="must"> *</span>
                       <span class="label label-danger" id="addressc"></span>
                        <input type="text" class="form-control" id="address" placeholder="Address*" name="address"/>
                    </div>
                </div>
            </div>
<!--            ..........................-->
            
            
            <div class="row">
            
            <div class='col-lg-6'>
            <div class="form-group">
            <label for=""  class="control-label ">Email</label>
            <span class="must"> *</span>
            <span class="label label-danger" id="emailc"></span>
            <input type="text" class="form-control" id="email" placeholder="Email*" name="email"/>
            </div>
            </div>
<!--            </div>-->
            
            
                <div class="col-lg-6">
                  
                   <div class="form group">
                        <span class="label label-danger" id="optradioc"></span>
                         <label class="control-label ">Gender : </label>
                           <span class="must"> *</span>
                            <label class="radio-inline">
                              <input type="radio" name="optradio" value="Male" id="optradio"/>Male
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="optradio" value="Female" id="optradio"/>Female
                            </label>
                        </div>
                     </div>
            </div>
               <div class="row">
                <div class="col-lg-6">
                       <div class="form-group">
                       <label for="">Blood group</label>
                       <span class="must"> *</span>
                       <span class="label label-danger" id="bloodc"></span>
<!--                              <label class="control-label col-lg-2">Blood Group* : </label>-->
                            
                                <select name="blood" id="blood"  class="form-control">
                                    <option value="">--Select Blood Group--</option>
                                    <option value="A+">A+</option>
                                    <option value="O+">O+</option>
                                    <option value="B+">B+</option>
                                    <option value="AB+">AB+</option>
                                    <option value="A-">A-</option>
                                    <option value="O-">O-</option>
                                    <option value="B-">B-</option>
                                    <option value="AB-">AB-</option>
                                </select>
                        </div>
                        
                </div>
                  <div class="col-lg-6">
                   <div class="form-group">
<!--						   <label class="control-label col-lg-2">Batch* : </label>-->
                             
                     <label for="">Select course</label>
                      <span class="must"> *</span>
                       <span class="label label-danger" id="batchc"></span>
                        <select name="batch" id="batch" class="form-control">
                            <option value="">--Select course--</option>
                            <option value="IFY">IFY</option>
                            <option value="L4DC">L4DC</option>
                            <option value="L5DC">L5DC</option>
                            <option value="BIT">BIT</option>
                        </select>
                     </div>
				</div>
           </div>
           
<!--      </div>-->
            
                 
            <div class="col-lg-12">
            <div class="row">
            <div class='col-lg-6'>
                <div class="form-group">
                <label for="">Password</label>
                <span class="must"> *</span>
                <span class="label label-danger" id="error1"></span>
                    <input type="password" class="form-control " placeholder="Password" name="password" id="password"/>
                </div>

            </div>
               
            <div class="col-lg-6">
                <div class="form-group">
                <label for="">Confirm password</label>
                <span class="must"> *</span>
                <span class="label label-danger" id="error"></span>
                <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword" id="cpassword"/>


                </div>
            </div>
            
            
            </div>  
                
            </div>
            
            
            <div class="col-lg-12">
                <div class="row">
                   
                <div class="col-lg-6">
                    <div class="form-group">
                                <label class="control-label">Facebook ID name : </label>
                                <input type="text"  class="form-control"  name="fcaebookidname" id="fcaebookidname"/>
                 
                    </div>
                    </div>
                    
                    
                   
                    <div class="col-lg-6">
                         <div class="form-group">
                                   <label class="control-label">Facebook profile link : </label>
                                <input type="text"  class="form-control" name="facebookprofilelink" id="facebookprofilelink"/>
                        </div>
                    </div>
                     
            
            </div>
      </div>
            
            <div class="col-lg-12">
                <div class="row">
                    <div class="form-group">
                    <div class="form-group show-border"> 
<!--                            <span class="btn btn-default btn-file">-->
                                <label class="control-label">Browse image  : </label>
                                <span class="must">*</span>
                                <span class='label label-danger' id="imagec"></span>
                                <span class="label label-primary">File type : jpeg, jpg, png || Max size : 30 kb</span>
                                <input class="form-control" id="image" name="image" type="file"/>
<!--                            </span>-->

<!--                        </div>-->
                </div>
            
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <div class="row">
                    <div class="form-group">
                         <label class="control-label">Reason of joining : </label>
                            <span class="must">*</span>
                            <span class='label label-danger' id="reasonofjoiningc"></span>
                            <textarea class="form-control" rows="5" name="reasonofjoining" id="reasonofjoining"></textarea>
                    </div>
                </div>
            </div>

           
            <div class="form-group">
            <button class="btn btn-primary btn-lg btn-block button" type="button" name="signup" id="button">Sign up</button>
            
            </div>
			
            <span> Go to <a href="loginform.html">Log in</a></span>
  </div>
  </div>
    </div>
     </div>
</form>
 
                        
                 
