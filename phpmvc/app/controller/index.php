<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/26/2017
 * Time: 2:31 PM
 */
class index extends Dcontroller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function home(){
        $this->load->view("home");
    }

    public function productList(){
        //$data=array();
        $pList=$this->load->model("ProductList");
        $data['product']=$pList->productList();
        $this->load->view("ProductList",$data);
    }

    public function PersonList(){
        $personLis=$this->load->model("Person");
        $data['person']=$personLis->getPerson();
        $this->load->view("Person",$data);
    }

    public function getMobile(){
        $mobile=$this->load->model("Mobile");
        $data['datum']=$mobile->getMobile();
        $this->load->view("Mobile",$data);
    }

    public function insertPerson(){
        $table="customer";
        $data=array(
            "Name"=>"New person",
            "Phn"=>"2341243",
            "Age"=>"90",
            "ID"=>25
        );
        $newPerson=$this->load->model("Person");
        $newPerson->inserNewPErson($table,$data);
    }
}