<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/26/2017
 * Time: 7:48 PM
 */
class Database extends PDO
{



    public function __construct()
    {
        $dsn="mysql:host=localhost;dbname=salesdata";
        $username="root";
        $passwd="";
        parent::__construct($dsn,$username,$passwd);
    }

    public function selectAllData($table){
        $sql="select * from $table";
        $stmt=$this->prepare($sql);
        $stmt->execute();
        $result=$stmt->fetchAll();
        return $result;
    }

    public function insert($table,$data){
        $keys=implode(",",array_keys($data));
        $values=":".implode(", :",array_keys($data));
        $sql="insert into $table VALUES ($values)";
        $stmt=$this->prepare($sql);
        foreach ($data as $key=>$val){
            $stmt->bindValue(":$key",$val);
        }
        return $stmt->execute();
    }

}