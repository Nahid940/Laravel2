<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/26/2017
 * Time: 2:48 PM
 */
class Load
{

    public function view($fileName,$data=null){
        if($data==true){
            extract($data);
        }
        include 'app/view/'.$fileName.'.php';

    }
    public function model($fileName){
        include 'app/model/'.$fileName.'.php';
        return new $fileName();
    }

}