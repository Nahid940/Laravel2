<?php

include_once 'system/libs/Main.php';
include_once 'system/libs/Dcontroller.php';
include_once 'system/libs/Load.php';
include_once 'system/libs/Database.php';
include_once 'system/libs/Dmodel.php';




if(isset($_GET['url'])){
    $url=$_GET['url'];
    $url=rtrim($url,'/');
    $url=explode('/',filter_var($url,FILTER_SANITIZE_URL));
    if(isset($url[0])){
        include_once 'app/controller/'.$url[0].'.php';
        $controller=new $url[0]();

        if(isset($url[2])){
            $controller->$url[1]($url[2]);
        }else{
            $controller->$url[1]();
        }
    }
}else{
    include_once 'app/controller/index.php';
    $index=new index();
    $index->home();
    $index->productList();
    $index->PersonList();
    $index->getMobile();

}



?>

