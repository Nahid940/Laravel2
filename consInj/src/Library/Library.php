<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 10/11/2017
 * Time: 10:01 PM
 */
namespace App\Library;
use App\Book\Book;

class Library
{

    protected $book;
    protected $libraryNo;
    public function __construct($librayNo,Book $book)
    {
        $this->book=$book;
        $this->libraryNo=$librayNo;

    }

    public function getBookName(){
        return $this->book;
    }
    public function getLibrary(){
        return $this->libraryNo;
    }

}