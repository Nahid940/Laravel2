<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 10/11/2017
 * Time: 10:00 PM
 */
namespace App\Book;
class Book
{

    protected $bookName;
    protected $bookPrice;
    protected $bookCategory;

    public function __construct($bookName,$bookPrice,$bookCategory)
    {
        $this->bookName=$bookName;
        $this->bookPrice=$bookPrice;
        $this->bookCategory=$bookCategory;
    }

    public function getBookName(){
        echo $this->bookName;
    }
    public function getBookPRice(){
        echo  $this->bookPrice;
    }

    public function getBookCategory(){
        echo $this->bookCategory;
    }



}