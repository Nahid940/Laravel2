<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/13/2017
 * Time: 9:42 PM
 */
namespace DBConfig;
class DBconfig
{
    private $dbName='myDB';

    public function getDbname(){
        return $this->dbName;
    }
}