<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('test','TestController');


Route::get('/', function () {
   // return view('welcome');
   return "Hello";
});




Route::get('/home', function () {
   // return view('welcome');
   return "Home";
});
Route::get('/about', function () {
   // return view('welcome');
   return "About";
});

Route::get('/contact', function () {
    return view('contact');
   
});
Route::get('/user', function () {
    return view('user');
   
});
Route::get('user/{id}/{name}', function ($id,$name) {
    return "User".$id.$name;
});
Route::get('home','basicController@index');
Route::get('user','basicController@user');
Route::get('about','basicController@about');


