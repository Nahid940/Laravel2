<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends Controller
{
    
	public function create(){
		return view('test.tests');
	}
	
	public function store(Request $request){
		//$name = $request->input('name');
		$input=$request->all();
		return $input;
	}
	
	 public function show($id)
    {
		return view('test.tests');
        //return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
