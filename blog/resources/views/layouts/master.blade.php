<html>
    <head>
        <title>App Name - @yield('title')</title>
		<style>
			.header{
				background:gray
			}
			.menubar{
				background:cyan
			}
			.container h1{
				color:red;
				background:black
			}
		</style>
    </head>
    <body>
	
		
		<div class="header">
			@yield('header')
			<hr>
		</div>
		
        <div class="menubar">
			@yield('menubar')
			<hr>
		</div>

        <div class="container">
            @yield('content')
			<hr>
        </div>
		
		<div class="footer">
            @yield('footer')
			<hr>
        </div>
    </body>
</html>