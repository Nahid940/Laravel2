<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
/*
            ul{
                margin: 0
            }
            li{
                float: left;
                display: inline-block;
                list-style: none;
                padding: 5px
            }
*/
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Hello dear</div>
                <ul>
                    <li><a href="">Ok</a></li>
                    <li><a href="">Hello</a></li>
                    <li><a href="">I am here</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>
