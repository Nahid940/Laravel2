<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('myPage/{id?}',function($id=null){
//    $data="$id";
//    return view('myPage',['data'=>$data]);
//});
Route::get('welcome',function (){
    return view('welcome');
});

//Route::get('myForm/form',function (){
//    return view('myForm.form');
//});

Route::get('insertData',function (){
   return view('insertData');
});

Route::get('myData',function (){
  return view('myData');
});

//Route::get('myForm/student',function (){
//  return view('myForm.student');
//});
Route::get('myForm/form','PageController@Form');
Route::get('myForm/employee','PageController@Employee');
Route::get('myForm/student','PageController@StudentForm');



//Route::get('myForm/edit/{student_id}','StudentController@edit');
Route::get('myForm/edit/{email}','StudentController@edit');



Route::get('myForm/student','StudentController@getAllStudent');

Route::post('myForm/register','RegisterController@register');
Route::post('AddPerson','MyDataController@AddPerson');
Route::post('PersonAge','PersonAge@PersonAge');


Route::post('myForm/employee/AddEmployee','EmployeeController@AddEmployee');
Route::post('myForm/student/AddStudent','StudentController@AddStudent');
Route::get('myForm/student/{uniqueid}','StudentController@delete');
Route::get('myForm/form','RegisterController@getAllData');
Route::get('myForm/form/{email}','RegisterController@getEmail');




