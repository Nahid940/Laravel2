@include('include.header')




<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 col-lg-offset-3">
    <form class="form-horizontal" method="post" action="{{url('myForm/employee/AddEmployee')}}">
        {{csrf_field()}}
        <fieldset>
            <legend>Registration form in laravel</legend>
            @if(session('employeeAdded'))
                <div class="alert alert-success">{{session('employeeAdded')}}</div>
            @endif

            @if(count($errors)>0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach
            @endif

            @if(session('response'))
                <div class="alert alert-success">{{session('response')}}</div>
            @endif
            <div class="form-group">
                <label for="name" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Age</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputAge" placeholder="Age" name="age">
                </div>
            </div>
            <div class="form-group">
                <label for="post" class="col-lg-2 control-label">Post</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputAge" placeholder="Post" name="post">
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
            $('.alert').delay(5000).fadeOut(1000, function () {
                $(this).alert('close');
            });
        });
</script>




@include('include.footer')

