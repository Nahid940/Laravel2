@include('include.header')




<div class="container">
    <form class="form-horizontal" method="post" action="{{url('myForm/register')}}">
        {{csrf_field()}}
        <fieldset>
            <legend>Registration form in laravel</legend>
            @if(count($errors)>0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger">{{$error}}</div>
            @endforeach
            @endif

            @if(session('response'))
                <div class="alert alert-success">{{session('response')}}</div>
            @endif
            <div class="form-group">
                <label for="name" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email">
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div class="col-lg-12">
<div class="row">

        <table class="table">
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Action</td>
            </tr>

            @foreach($data as $value)
                <tr>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td><a href="{{'form/'.$value->email}}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

@include('include.footer')

