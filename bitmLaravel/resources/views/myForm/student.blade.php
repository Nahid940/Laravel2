@include('include.header')



    <div class="col-md-12">
        <div class="row">
    <div class="col-md-6 col-lg-offset-3">
        @if(count($errors)>0)
        @foreach($errors->all() as $erros)
            <div class="alert alert-warning">{{$erros}}</div>
        @endforeach
        @endif

        @if(session('inserted'))
            <div class="alert alert-success">{{session('inserted')}}</div>
            @endif

        <form name="studentForm" class="form-horizontal" method="post" action="{{url('myForm/student/AddStudent')}}" onsubmit=" return validateForm()">
            {{ csrf_field() }}


            <fieldset>
                <legend>Student form</legend>

                <div class="form-group">
                    <label for="inputName" class="col-lg-2">Name</label>

                    <div class="col-lg-10">
                        <span id="NameValidate"></span>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2">Email</label>
                    <div class="col-lg-10">
                        <span id="EmailValidate"></span>
                        <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email">
                    </div>
                </div>

                <div class="form-group">
                    <label for="course" class="col-lg-2">Course</label>
                    <div class="col-lg-10">
                        <span id="CourseValidate"></span>
                        <input type="text" class="form-control" id="inputEmail" placeholder="Course" name="course">
                    </div>
                </div>
                <div class="form-group pull-right" >
                    <input type="submit" class="btn btn-success">
                </div>
            </fieldset>
        </form>
    </div>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <table class="table">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Email</td>
                <td>Course</td>
                <td>Delete</td>
                <td>Edit</td>
            </tr>

            @if(empty($data))
                <div class="alert alert-warning">No data found !!</div>
            @else

            @foreach($data as $value)
                <tr>
                    <td>{{$value->student_id}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->course}}</td>
                    <td><a href="{{url('myForm/student/'.$value->uniqueid)}}" class="btn btn-danger">Delete</a></td>
{{--                    <td><a href="{{url('myForm/edit/'.$value->student_id)}}" class="btn btn-primary">Edit</a></td>--}}
                    <td><a href="{{url('myForm/edit/'.$value->student_id)}}" class="btn btn-primary">Edit</a></td>
                </tr>
            @endforeach
                @endif
        </table>
    </div>
</div>

{{--<script>--}}

    {{--function validateForm(){--}}

        {{--var name = document.forms["studentForm"]["name"].value;--}}
        {{--var email = document.forms["studentForm"]["email"].value;--}}
        {{--var course = document.forms["studentForm"]["course"].value;--}}

        {{--if(name==''){--}}
            {{--$('#NameValidate').text("Enter name");--}}
            {{--$('#NameValidate').css("color","red");--}}
            {{--return false;--}}
        {{--}else if(email==''){--}}
            {{--$('#EmailValidate').text("Enter email");--}}
            {{--$('#EmailValidate').css("color","red");--}}
            {{--return false;--}}
        {{--}else if(course==''){--}}
            {{--$('#CourseValidate').text("Enter course");--}}
            {{--$('#CourseValidate').css("color","red");--}}
            {{--return false;--}}
        {{--}--}}
    {{--}--}}
{{--</script>--}}


@include('include.footer')