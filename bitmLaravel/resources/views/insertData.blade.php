@include('include.header')


<div class="container">

    <div class="row">
        <div class="col-md-6 offset-2">

            <form action="{{url('AddPerson')}}" method="post">
                {{csrf_field()}}
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif

                @if(session('complete'))
                    <div class="alert alert-success">{{session('complete')}}</div>
                @endif

                <div class="form-group">
                    <label class="control-label" for="focusedInput">Name</label>
                    <input class="form-control" id="focusedInput" type="text" name="name">
                </div>

                <div class="form-group">
                    <label class="control-label" for="address">Address</label>
                    <input class="form-control" id="disabledInput" type="text"  name="address">
                </div>

                <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-success">
                </div>
            </form>

        </div>
    </div>
</div>


@include('include.footer')