<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $primaryKey='student_id';
//    protected $primaryKey='uniqueid';
//    protected $email='email';
    protected $dates=['deleted_at'];
    //

//    public function getAllStudents(){
//        $data=Student::all();
//        return view('myForm/form',compact('data'));
//    }
}
