<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //

    public function AddEmployee(Request $request){
        $this->validate(
            $request,[
                "name"=>"required",
                "email"=>"required",
                "age"=>"required",
                "post"=>"required"
            ]
        );

        $employee=new Employee();
        $employee->name=$request->input('name');
        $employee->email=$request->input('email');
        $employee->age=$request->input('age');
        $employee->post=$request->input('post');
        $employee->save();
        return redirect('myForm/employee')->with('employeeAdded',"Employee data inserted !!");

    }
}
