<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonAge extends Controller
{
    //

    public function PersonAge(Request $request){
        $this->validate(
            $request,[
                "name"=>"required",
                "age"=>"required"
            ]
        );

        $pAge=new PersonAge();
        $pAge->name=$request->input('name');
        $pAge->age=$request->input('age');
        return redirect('myData')->with('complete',"Data recorded !!");


    }
}
