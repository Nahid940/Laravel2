<?php

namespace App\Http\Controllers;

use App\registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    public function register(Request $request){
        $this->validate(
            $request,[
                "name"=>"required",
                "email"=>"required"
        ]
        );
        $reg=new Registration();
        $reg->name=$request->input('name');
        $reg->email=$request->input('email');
        $reg->save();
        return redirect('myForm/form')->with('response','Registration successful !!');
    }

    public function getAllData(){
        $data=Registration::all();
        if(count($data)>0){
            return view('myForm/form',compact('data'));
        }else{
            $data="No data found";
            return view('myForm/form',compact('data'));
        }
    }

    public function getEmail($email){
        echo "The email is $email";
    }



}
