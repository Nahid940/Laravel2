<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

class MyDataController extends Controller
{
    //


    public function AddPerson(Request $request){
        $this->validate(
            $request,[
                "name"=>"required",
                "address"=>"required"
            ]
        );

        $person=new Person();
        $person->name=$request->input('name');
        $person->address=$request->input('address');
        $person->save();
        return redirect('/insertData')->with('complete','Data inserted!!');
    }
}
