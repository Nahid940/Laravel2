<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    //

    public function AddStudent(StudentRequest $request){


        $student=new Student();
        $student->name=$request->input('name');
        $student->email=$request->input('email');
        $student->course=$request->input('course');
        $student->uniqueid=(md5(time()));
        $student->save();
        return redirect('myForm/student')->with("inserted","Data inserted successfully");
    }
    public function getAllStudent(){
        $data=Student::all();
        if(count($data)>0){
            return view('myForm/student',compact('data'));
        }else{
            return view('myForm/student');
        }
    }

//    public function edit($student_id){
//        $studentd=Student::find($student_id);
//        return view('myForm/edit',compact('studentd'));
//    }
    public function edit($student_id){
        $studentd=Student::find($student_id);
        return view('myForm/edit',compact('studentd'));
//        dd($studentd);
    }

    public function delete($uniqueid){
        Student::where('uniqueid',$uniqueid)->delete();
        return redirect('myForm/student')->with('deleted',"Data deleted");
    }

}
