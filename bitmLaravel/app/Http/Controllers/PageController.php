<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //

        public function StudentForm(){
            return view('myForm.student');
        }

      public function Form(){
            return view('myForm.form');
        }

        public function Employee(){
            return view ('myForm.employee');
        }
}
