<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('admin/','EmployeeController@index');
Route::post('admin/store','EmployeeController@store');
Route::get('admin/list','EmployeeController@view');
Route::post('admin/destroy','EmployeeController@destroy');
Route::get('admin/restore/{id}','EmployeeController@restore');
