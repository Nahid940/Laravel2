<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $total=Employee::count();
        return view('admin/add',compact('total'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $data=Employee::paginate(5);
        $trashData=Employee::onlyTrashed()->get();
        return view('admin/list',compact('data','trashData'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
//        Employee::create();

        if($request->ajax()){
            $validData=$this->validate($request,[
                'name'=>'required',
                'age'=>'required',
            ]);

            $total=Employee::count();

            Employee::create($validData);
            return response()->json([
                'response'=>'Data inserted',
                'total'=>$total
            ]);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id)
//    {
//        $id=$_POST['id'];
//        Employee::destroy($id);
//
//
//    }
    public function destroy()
    {
        $id=$_POST['id'];
        $emp=Employee::find($id);
        $emp->delete();
        return response()->json('Data deleted !!');
    }

    public function restore($id){
        $res=Employee::onlyTrashed()->find($id);
        $res->restore();
        return redirect('admin/list');
    }

}
