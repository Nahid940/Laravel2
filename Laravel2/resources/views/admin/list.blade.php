<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document</title>
    <link rel="stylesheet" href="{{asset('backend/css/bootstrap.min.css')}}">
    <style>

    </style>
</head>
<body>



<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{url('admin/')}}">Home<span class="sr-only">(current)</span></a></li>
                    <li><a href="#">Employee list</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>
        </div>
    </nav>



    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-6 col-lg-offset-3">
                <table class="table table-striped table-hover ">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    @foreach($data as $d)
                    <tbody>
                    <tr class="records">
                        <td>1</td>
                        <td>{{$d->name}}</td>
                        <td>{{$d->age}}</td>
                        <td><a href="#" onclick="Delete('{{$d->id}}')" class="btn btn-danger">Delete</a></td>
                        <td><a href="#"  class="btn btn-info">Edit</a></td>
                    </tr>
                    </tbody>
                        @endforeach
                </table>

                {{$data->links()}}


                <hr/>

                <h2>Trashed data</h2>


                <table class="table table-striped table-hover ">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    @foreach($trashData as $d)
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->age}}</td>
                            <td><a href="#" onclick="Delete('{{$d->id}}')" class="btn btn-danger">Delete permanently</a></td>
                            <td><a href="{{url('admin/restore/'.$d->id)}}"  class="btn btn-success">Restore</a></td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
{{--                {{$trashData->links()}}--}}
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {

        function timeOut(){
            setTimeout(function(){
                timeOut();
            },5000);
        }


    });

    function Delete(i){

        $.ajax({
            type:'POST',
            data:{
              'id':i,
            },
            dataType:'JSON',
            url:"<?=URL::to('admin/destroy')?>",
            success:function (response) {

            }

        });

    }

</script>

</body>
</html>