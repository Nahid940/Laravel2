<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document</title>
    <link rel="stylesheet" href="{{asset('backend/css/bootstrap.min.css')}}">
</head>
<body>

<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{url('admin/')}}">Home<span class="sr-only">(current)</span></a></li>
                    <li><a href="{{url('admin/list')}}">Employee list</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Total employee <span class="badge badge-notify" id="total" style="background-color: #ff2d3f;color: #fff">{{$total}} </span> </a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-6 col-lg-offset-3">
                {{--<span class="label label-success" id="datainsert"></span>--}}
                <div class="alert alert-success" id="datainsert"></div>
                <form action="{{url('admin/store')}}" method="post" id="employeeForm">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="control-label" for="name">Name</label>
                        <input class="form-control" id="name" type="text" name="name">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="age">Age</label>
                        <input type="text" class="form-control" id="age" name="age">
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script>

    $(document).ready(function () {
        $('#datainsert').hide();



    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#employeeForm').on('submit',function (e) {
//        alert('ok');
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var post = $(this).attr('method');


        $.ajax({
            url:url,
            data:data,
            type:post,
            dataType:'JSON',
            success:function (response) {
                $('#datainsert').show();
                $('#datainsert').text(response['response']);
            },
            error:function () {

            }
        });
    });

</script>

</body>
</html>