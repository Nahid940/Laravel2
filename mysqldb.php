<?php
error_reporting(0);
class mysqldb {
var $link;
var $result;
    
    function connect() {
    $this->link = mysqli_connect("localhost","root","","user-login");

    if($this->link) {
        mysqli_query($this->link,"SET NAMES 'utf-8'");
        return true;
    }
        
    $this->show_error(mysqli_error($this->link));
    return false;
    }
    
    
        function selectdb($database) {
            if($this->link) {
            //mysql_select_db($database, $this->link);
            mysqli_select_db($this->link,$database);
            return true;
        }

        $this->show_error("Not connect the database before", "selectdb($database)");
        return false;
        }
    
        function query($sql) {
            $this->query = mysqli_query($this->link,$sql);
            return $this->query;
        }
    
        function fetch() {
        $result = mysqli_fetch_object($this->query);
        return $result;
        }
    
        function num_rows() {
        return mysqli_num_rows($this->query); 
        }
    
        function show_error($errmsg, $func) {
        echo "<b><font color=red>" . $func . "</font></b> : " . $errmsg . "<BR>\n";
        exit(1);
    } 
}
?>