<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title>Apartment - Premium Real Estate HMTL Site Template</title>
	<link rel="stylesheet" href="assets/css-front/custom-style.css">
	
	<link rel="shortcut icon" href="http://apartment-html.chart.civ.pl/favicon.ico" type="image/x-icon">
	<link rel="icon" href="http://apartment-html.chart.civ.pl/favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://apartment-html.chart.civ.pl/bootstrap/bootstrap.min.css">    
	<!-- Font awesome styles -->    
	<link rel="stylesheet" href="http://apartment-html.chart.civ.pl/apartment-font/css/font-awesome.min.css">  
	<!-- Custom styles -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,500italic,700,700italic&amp;subset=latin,latin-ext'>
	<link rel="stylesheet" type="text/css" href="http://apartment-html.chart.civ.pl/css/plugins.css">
    <link rel="stylesheet" type="text/css" href="http://apartment-html.chart.civ.pl/css/apartment-layout.css">
    <link id="skin" rel="stylesheet" type="text/css" href="http://apartment-html.chart.civ.pl/css/apartment-colors-blue.css">
	<style>
	#switcher-button{
		display:none
	}
	</style>
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>
<body>
<div class="loader-bg"></div>
<a id="start"></a>
<div id="wrapper" class="wrapper2">

<!-- Page header -->	
	<header class="header3">
		<nav class="navbar main-menu-cont">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar icon-bar1"></span>
						<span class="icon-bar icon-bar2"></span>
						<span class="icon-bar icon-bar3"></span>
					</button>
					<a href="#" title="" class="navbar-brand">
						<img src="#" alt="Apartment" />
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#start" class="scroll">Home</a></li>
						<li><a href="#overview" class="scroll">Overview</a></li>
						
						<li><a href="#gallery" class="scroll">Gallery</a></li>
						<li><a href="#available" class="scroll">Available</a></li>
						
						<li><a href="#contact" class="special-color scroll">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav><!-- /.mani-menu-cont -->	
    </header>
	
    <section class="no-padding adv-search-section">
		<!-- Slider main container -->
		<div id="swiper2" class="swiper-container">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
				<!-- Slides -->
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/1.jpg">
					
				</div>
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/5.jpg">
					
				</div>
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/3.jpg">
					
				</div>
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/4.jpg">
					
				</div>
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/8.jpg">
					
				</div>
				<div class="swiper-slide swiper-lazy" data-background="http://apartment-html.chart.civ.pl/images/slides/10.jpg">
					
				</div>
			</div>
		</div>
	</section>
	
	<a id="overview"></a>
    

	

	<a id="rooms"></a>	
    <section class="rooms parallax">
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-12 text-center">
					<h5 class="subtitle-margin">property</h5>
							<h1 class="">details<span class="special-color">.</span></h1>
				</div>
				<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
					<div class="title-separator-primary2"></div>
				</div>
			</div>
		</div>
		<div class="container margin-top-60">
			<div class="row">
				<div class="col-xs-12 col-lg-10 col-lg-offset-1">
					<div class="panel-group panel-apartment" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<span>LIVING ROOM</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details1.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<span>DINING ROOM</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details2.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<span>KITCHEN</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details3.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFour">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
									<span>BEDROOM 1</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details4.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFive">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
									<span>BEDROOM 2</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details5.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingSix">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
									<span>BEDROOM 3</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details6.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingSeven">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
									<span>BATHROOMS</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details7.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingEight">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
									<span>GARAGE</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon"><i class="jfont">&#xe801;</i></div>
								</a>
							</div>
							<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<img src="http://apartment-html.chart.civ.pl/images/room-details8.jpg" alt="" class="p-image img-responsive" />
										</div>
										<div class="col-xs-12 col-sm-6">	
											<p class="negative-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											<div class="row margin-top-30">
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
												<div class="col-xs-6">
													<ul class="ticks-ul">
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
														<li><i class="jfont">&#xe815;</i>nostrud exercitation</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>	
				</div>
			</div>
		</div>
	</section>
	
	<a id="gallery"></a>
	<section class="section-light section-both-shadow">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-12 text-center">
					<h5 class="subtitle-margin">property</h5>
							<h1 class="">photo gallery<span class="special-color">.</span></h1>
				</div>
				<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
					<div class="title-separator-primary2"></div>
				</div>
			</div>
		</div>
		<div class="container gallery-filter-cont margin-top-60">
			<div class="row">
				<div class="col-xs-12 text-center">
					<div class="gallery-filter" data-filter="*">All</div>
					<div class="gallery-filter" data-filter=".living">LIVING ROOM</div>
					<div class="gallery-filter" data-filter=".dining">DINING ROOM</div>
					<div class="gallery-filter" data-filter=".kitchen">KITCHEN</div>
					<div class="gallery-filter" data-filter=".bedroom1">BEDROOM 1</div>
					<div class="gallery-filter" data-filter=".bedroom2">BEDROOM 2</div>
					<div class="gallery-filter" data-filter=".bedroom3">BEDROOM 3</div>
					<div class="gallery-filter" data-filter=".bathrooms">BATHROOMS</div>
					<div class="gallery-filter" data-filter=".garage">GARAGE</div>
				</div>
			</div>
		</div>
		
		<div class="gallery-grid">
			<div class="gallery-grid-sizer"></div>
			<div class="gallery-grid-lg">
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery1.jpg" class="gallery-grid-item living dining bedroom2" data-sub-html="Living room"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery1-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery2.jpg" class="gallery-grid-item dining garage bedroom3" data-sub-html="Dining room"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery2-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery3.jpg" class="gallery-grid-item kitchen living bedroom3" data-sub-html="Living room"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery3-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery4.jpg" class="gallery-grid-item bedroom1 kitchen dining" data-sub-html="Kitchen"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery4-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery5.jpg" class="gallery-grid-item living bedroom1 garage" data-sub-html="Bedroom 1"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery5-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery6.jpg" class="gallery-grid-item dining bedroom3 bathrooms" data-sub-html="Bedroom 2"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery6-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery7.jpg" class="gallery-grid-item kitchen bedroom2" data-sub-html="Bedroom 3"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery7-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
				<a href="http://apartment-html.chart.civ.pl/images/gallery/gallery8.jpg" class="gallery-grid-item bedroom1 bedroom2 bathrooms" data-sub-html="Bathroom"><img src="http://apartment-html.chart.civ.pl/images/gallery/gallery8-thumb.jpg" alt="" /><span>living room<br/><i class="fa fa-search-plus"></i></span><div class="big-triangle"></div></a>
			</div>
		</div>

	</section>
	<a id="available"></a>
	<section>
		<div class="all container">
	<h2 style="margin-bottom:28px; color:#fff;">Available Apartments</h2>
	<div class="box">
      <div class="box_aside">
        <img src="assets/images/page_img02.jpg" alt="">
      </div>
      <div class="box_cnt">
        <h6 class="box_cnt_title"><a href="#">A1 F3</a><a class="add-icon" href="#"></a></h6>
	        <table>
		         <tbody>
		          	<tr>
		            	<td class="ta-left">Floor No.: </td>
		            	<td class="ta-right col-primary">2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Rooms: </td>
			            <td class="ta-right col-primary">3</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Area: </td>
			            <td class="ta-right col-primary">98 m2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Price: </td>
			            <td class="ta-right col"><span>$89,900</span></td>
			         </tr>
			         <tr>
			            <td><a class="btn btn-primary cen" href="http://localhost/project/assets/AdminLogReg.php">Book Now</a></td>
			            
			         </tr>
		        </tbody>
    		</table>
      </div>
    </div>
    <div class="box">
      <div class="box_aside">
        <img src="assets/images/page_img01.jpg" alt="">
      </div>
      <div class="box_cnt">
        <h6 class="box_cnt_title"><a href="#">A1 F3</a><a class="add-icon" href="#"></a></h6>
	        <table>
		         <tbody>
		          	<tr>
		            	<td class="ta-left">Floor No.: </td>
		            	<td class="ta-right col-primary">2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Rooms: </td>
			            <td class="ta-right col-primary">3</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Area: </td>
			            <td class="ta-right col-primary">98 m2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Price: </td>
			            <td class="ta-right col"><span>$89,900</span></td>
			         </tr>
			         <tr>
			            <td><a class="btn btn-primary cen" href="http://localhost/project/assets/AdminLogReg.php">Book Now</a></td>
			            
			         </tr>
		        </tbody>
    		</table>
      </div>
    </div>
    <div class="box">
      <div class="box_aside">
        <img src="assets/images/page_img03.jpg" alt="">
      </div>
      <div class="box_cnt">
        <h6 class="box_cnt_title"><a href="#">A1 F3</a><a class="add-icon" href="#"></a></h6>
	        <table>
		         <tbody>
		          	<tr>
		            	<td class="ta-left">Floor No.: </td>
		            	<td class="ta-right col-primary">2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Rooms: </td>
			            <td class="ta-right col-primary">3</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Area: </td>
			            <td class="ta-right col-primary">98 m2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Price: </td>
			            <td class="ta-right col"><span>$89,900</span></td>
			         </tr>
			         <tr>
			            <td><a class="btn btn-primary cen" href="http://localhost/project/assets/AdminLogReg.php">Book Now</a></td>
			            
			         </tr>
		        </tbody>
    		</table>
      </div>
    </div>
    <div class="box">
      <div class="box_aside">
        <img src="assets/images/page_img04.jpg" alt="">
      </div>
      <div class="box_cnt">
        <h6 class="box_cnt_title"><a href="#">A1 F3</a><a class="add-icon" href="#"></a></h6>
	        <table>
		         <tbody>
		          	<tr>
		            	<td class="ta-left">Floor No.: </td>
		            	<td class="ta-right col-primary">2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Rooms: </td>
			            <td class="ta-right col-primary">3</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Area: </td>
			            <td class="ta-right col-primary">98 m2</td>
			         </tr>
			         <tr>
			            <td class="ta-left">Price: </td>
			            <td class="ta-right col"><span>$89,900</span></td>
			         </tr>
			         <tr>
			            <td><a class="btn btn-primary cen" href="http://localhost/project/assets/AdminLogReg.php">Book Now</a></td>
			         </tr>
		        </tbody>
    		</table>
      </div>
    </div>
</div>
	
	</section>
	
	
	
	
	<a id="localization"></a>
    <section class="contact-page-1">
			<div>
				<a id="contact"></a>
				<div class="contact3 wow fadeInUp">
								<div class="row">
									<div class="col-xs-12 col-lg-12 text-center">
										<h5 class="subtitle-margin">contact the agent</h5>
										<h1 class="">Shedule a visit<span class="special-color">.</span></h1>
									</div>
									<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
										<div class="title-separator-primary2"></div>
									</div>
								</div>
								<div class="row margin-top-60">
									<div class="col-xs-10 col-sm-4 col-xs-offset-1 col-sm-offset-0">
										<h5 class="subtitle-margin">Manager</h5>
										<h3 class="title-negative-margin">Mark Smith<span class="special-color">.</span></h3>
										<a href="http://apartment-html.chart.civ.pl/agent-right-sidebar.html" class="agent-photo">
											<img src="http://apartment-html.chart.civ.pl/images/agent3.jpg" alt="" class="img-responsive" />
										</a>
									</div>
									<div class="col-xs-12 col-sm-8">
										<div class="agent-social-bar">
											<div class="pull-left">
												<span class="agent-icon-circle">
													<i class="fa fa-phone"></i>
												</span>
												<span class="agent-bar-text">123-456-789</span>
											</div>
											<div class="pull-left">
												<span class="agent-icon-circle">
													<i class="fa fa-envelope fa-sm"></i>
												</span>
												<span class="agent-bar-text">apartment@touchdevs.com</span>
											</div>
											<div class="pull-right">
												<div class="pull-right">
													<a class="agent-icon-circle" href="#">
														<i class="fa fa-facebook"></i>
													</a>
												</div>
												<div class="pull-right">
													<a class="agent-icon-circle icon-margin" href="#">
														<i class="fa fa-twitter"></i>
													</a>
												</div>
												<div class="pull-right">
													<a class="agent-icon-circle icon-margin" href="#">
														<i class="fa fa-google-plus"></i>
													</a>
												</div>
												<div class="pull-right">
													<a class="agent-icon-circle icon-margin" href="#">
														<i class="fa fa-skype"></i>
													</a>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<form name="contact-from" id="contact-form" action="#" method="get">
											<div id="form-result"></div>
											<input name="name" id="name" type="text" class="input-short main-input" placeholder="Your name" />
											<input name="phone" id="phone" type="text" class="input-short pull-right main-input" placeholder="Your phone" />
											<input name="mail" id="mail" type="email" class="input-full main-input" placeholder="Your email" />
											<textarea name="message" id="message" class="input-full agent-textarea main-input" placeholder="Your question"></textarea>
											<div class="form-submit-cont">
												<a href="#" class="button-primary pull-right" id="form-submit">
													<span>send</span>
													<div class="button-triangle"></div>
													<div class="button-triangle2"></div>
													<div class="button-icon"><i class="fa fa-paper-plane"></i></div>
												</a>
												<div class="clearfix"></div>
											</div>
										</form>
									</div>
								</div>
							</div>	
			</div>
    </section>
		
	<footer class="small-cont">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6 small-cont">
					<img src="#" alt="Appartment" class="img-responsive footer-logo" />
				</div>
				<div class="col-xs-12 col-md-6 footer-copyrights">
					&copy; Copyright 2017 <a href="touchdevs.com" target="blank">Touchdevs</a>. All rights reserved.<a href="http://touchdevs.com" target="blank">Touchdevs</a>.
				</div>
			</div>
		</div>
	</footer>
</div>	

<!-- Move to top button -->

<div class="move-top">
	<div class="big-triangle-second-color"></div>
	<div class="big-icon-second-color"><i class="jfont">TOP</i></div>
</div>	

<!-- jQuery  -->
    <script type="text/javascript" src="http://apartment-html.chart.civ.pl/js/jQuery/jquery.min.js"></script>
	<script type="text/javascript" src="http://apartment-html.chart.civ.pl/js/jQuery/jquery-ui.min.js"></script>
	
<!-- Bootstrap-->
    <script type="text/javascript" src="http://apartment-html.chart.civ.pl/bootstrap/bootstrap.min.js"></script>

<!-- Google Maps -->
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDfDCV5hXiPamCIT8_vwGXuzimLQ9MF76g&amp;sensor=false&amp;libraries=places"></script>
	
<!-- plugins script -->
	<script type="text/javascript" src="http://apartment-html.chart.civ.pl/js/plugins.js"></script>

<!-- template scripts -->
	<!-- <script type="text/javascript" src="http://apartment-html.chart.civ.pl/mail/validate.js"></script> -->
    <script type="text/javascript" src="http://apartment-html.chart.civ.pl/js/apartment.js"></script>
	
	</body>
</html>