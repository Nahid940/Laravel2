
@extends('admin.layout.master')

@section('content')

    <div class="row">

        <h2>Update info</h2>

        <div class="col-lg-12">
            <div class="col-lg-6 col-lg-offset-3">

                @if(session('insert'))
                    <div class="alert alert-success">{{session('insert')}}</div>
                @endif


                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif

                {{--<form method="post" action="{{url('admin/store')}}">--}}

                {!! Form::open(['url'=>'admin/'.$data->id,'method'=>'patch']) !!}
                {{csrf_field()}}
                <div class="form-group">

                    {!! Form::label('Name') !!}
                    {!! Form::text('name',$data->name,['class'=>'form-control']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('Email : ') !!}
                    {!! Form::email('email',$data->email,['class'=>'form-control']) !!}

                </div>

                    <div class="form-group">
                    {!! Form::label('Age : ') !!}
                    {!! Form::text('age',$data->age,['class'=>'form-control']) !!}

                </div>

                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>

    </div>

@endsection