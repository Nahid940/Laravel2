@extends('admin.layout.master')

@section('content')

    <h2>View data</h2>


    @if(session('update'))
        <div class="alert alert-info">{{session('update')}}</div>
    @endif
    @if(session('delete'))
        <div class="alert alert-warning">{{session('delete')}}</div>
    @endif


    <table class="table">

        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Age</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        @foreach($data as $d)

            <tbody>
            <tr>
                <td>{{$d->name}}</td>
                <td>{{$d->email}}</td>
                <td>{{$d->age}}</td>
                <td><a href="{{url('admin/editStudent/'.$d->id)}}" class="btn btn-info">Edit</a>

                </td>
                <td>
                    {!! Form::open(['url'=>'admin/destroy'.$d->id,'method'=>'delete']) !!}

                    {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}

                    {!! Form::close() !!}
                </td>

            </tr>
            </tbody>
        @endforeach

    </table>


@endsection
