@extends('admin.layout.master')

@section('content')

    <div class="col-lg-12">
        <div class="col-lg-6 col-lg-offset-3">

            @if(session('insert'))
                <div class="alert alert-success">{{session('insert')}}</div>
            @endif


            @if(count($errors)>0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach
            @endif

            {!! Form::open(['url'=>'admin/store','method'=>'POST']) !!}
            {{csrf_field()}}
            <div class="form-group">
                <label for="email">Name:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="age">Age:</label>
                <input type="text" class="form-control" id="age" name="age">
            </div>

            <div class="form-group pull-right">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>


            {{--</form>--}}
            {!! Form::close() !!}
        </div>
    </div>

@endsection