<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/addStudent');
    }

    public function viewAll(){
        $data=Student::all();

        return view('admin/viewStudent',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validateData=$this->validate($request,[
           'name'=>'required',
            'email'=>'required',
            'age'=>'required'
        ]);

        Student::create($validateData);
        return redirect('admin/addStudent')->with('insert','New Student info added !!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $data=Student::find($id);
        return view('admin/editStudent',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validaateData=$this->validate(
            $request,[
           "name"=>'required',
           "email"=>'required',
           "age"=>'required',
        ]);

        $student=Student::find($id);

//        dd($student);

        $student->update($validaateData);
        return redirect('admin/viewStudent')->with('update','Data updated !! ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Student::destroy($id);
        return redirect('admin/viewStudent')->with('delete','Data deleted !!');
    }
}
