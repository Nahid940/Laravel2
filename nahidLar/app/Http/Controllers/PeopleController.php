<?php

namespace App\Http\Controllers;

use App\People;
use App\Person;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin/insert');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function viewData(){

        $people= new People();
        $data=$people->all();

        return view('admin/view',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validateData=$this->validate(
            $request,[
                'name'=>'required',
                'email'=>'required'
            ]
        );

//        $people=new People();
//        $people->name=$request->input('name');
//        $people->email=$request->input('email');
//        $people->save();

        People::create($validateData);
        return redirect('admin')->with('insert',"Data Inserted !!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $people=new People();
        $data=$people->find($id);
        return view('admin/edit',compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

//

        $validateData=$this->validate(
            $request,[
                'name'=>'required',
                'email'=>'required'
            ]
        );
        $people=People::find($id);
        $people->update($validateData);
        return redirect('admin/view')->with('update',"Data updated");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        People::destroy($id);
        return redirect('admin/view')->with('delete',"Data deleted !!");
    }
}
