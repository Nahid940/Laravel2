<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','HomeController@welcome');
//Route::get('MyPage/mypage/{id}','HomeController@mypage');



//Route::get('/MyPage/mypage/{id}',function ($id){
//    return view('Mypage/mypage',compact('id'));
//});

//Route::get('/',function (){
//    return view('index');
//});

//Route::get('admin','PeopleController');

use Illuminate\Support\Facades\Route;

//Route::resource('admin','PeopleController');
Route::get('admin','PeopleController@index');

Route::get('admin/view','PeopleController@viewData');
Route::post('admin/store','PeopleController@store');
Route::get('admin/edit/{id}','PeopleController@edit');
Route::patch('admin/{id}','PeopleController@update');
Route::delete('admin/{id}','PeopleController@destroy');


Route::get('admin/addStudent','StudentController@index');
Route::post('admin/store','StudentController@store');
Route::get('admin/viewStudent','StudentController@viewAll');
Route::post('admin/','StudentController@destroy');
Route::get('admin/editStudent/{id}','StudentController@edit');

Route::patch('admin/{id}','StudentController@update');