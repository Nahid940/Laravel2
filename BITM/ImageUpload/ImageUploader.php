<?php
//session_start();
include_once('Message.php');
//use Message;
//use utility;
//use PDO;

class ImageUploader{
    public $name="";
    public $image="";
    public $username="root";
    public $password="";
    public $con="";
    
    public function __construct(){
        try{
            $this->con=new PDO('mysql:host=localhost;dbname=imageupload',$this->username,$this->password);
            $this->con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exp){
            echo 'Error'.$exp->getMessage();
        }
    }
    
    
    public function prepare1($data=array()){
        if(is_array($data) && array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        
        if(is_array($data) && array_key_exists('image',$data)){
            $this->image=$data['image'];
        }
        
        return $this;
    }
    
    
    
    
    public function store(){
        if(!empty($this->name) && !empty($this->image)){
            $stmt="insert into uploadimage(name,profilepic) values(:name,:image)";
            $q=$this->con->prepare($stmt);
            $q->execute(array(':name'=>$this->name,':image'=>$this->image));
        }
        
        if($q){
            Message::messages("Profile data inserted");
            header('location:index.php');
            //Utility::redirect();
        }else{
            Message::messages("Data error");
            header('location:create.php');
        }
    }
    
    
    
       
    public function index(){
        $data=array();
        $sql="select * from uploadimage";
        $q=$this->con->query($sql);
        while($r=$q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$r;
        }
        return $data;
        
    }
    
    
    
    
    
}
?>