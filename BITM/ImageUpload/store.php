<?php
include('ImageUploader.php');
if(isset($_FILES['image'])){
    $errors=array();
    $file_name=time().$_FILES['image']['name'];
    $file_size=$_FILES['image']['size'];
    $file_temp=$_FILES['image']['tmp_name'];
    $file_type=$_FILES['image']['type'];
    $File_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
    
    $formats=array('jpg','jpeg','png');
    
    
    if(in_array($File_ext,$formats)==false){
        $errors[]="File format not supported";
    }
    
    if($file_size>2097152){
         $errors[]="Large file size !!";
    }
    
    if(empty($errors)==true){
        
        move_uploaded_file($file_temp,"uploads/".$file_name);
        $_POST['image']="uploads/".$file_name;
    }else{
        print_r($errors);
    }
    
    
    $profile=new ImageUploader();
    $profile->prepare1($_POST)->store();
}

?>