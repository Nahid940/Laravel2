<?php

 if(!isset($_SESSION)){
        session_start();
    }
class Message{
   public static function messages($message=null){
       if(is_null($message)){
           $_message=self::getMessage();
           return $_message;
       }else{
           self::setMessage($message);
       }
   }
    
    
    public static function setMessage($message){
        $_SESSION['message']=$message;
    }
    
    public static function getMessage(){
        $_message=$_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
    }
    
    
}
?>