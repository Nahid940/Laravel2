<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/16/2017
 * Time: 9:56 PM
 */
namespace Abstraction;
abstract class Grade
{
    public $grade;
    abstract public function distinction();
    abstract public function merit();
    abstract public function pass();
    abstract public function fail();
}