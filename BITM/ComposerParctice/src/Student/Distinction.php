<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/16/2017
 * Time: 10:00 PM
 */

namespace App\student;




use Abstraction\Grade;

class Distinction extends Grade
{
    public function distinction()
    {
        echo "The result is distinction";
    }
    public function merit()
    {
        // TODO: Implement merit() method.
        echo "The grade is merit";
    }
    public function pass()
    {
        // TODO: Implement pass() method.
        echo "The grade is pass";
    }

    public function fail()
    {
        // TODO: Implement fail() method.
        echo "The grade is fail";
    }
}