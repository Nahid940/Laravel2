<?php
include_once 'vendor/autoload.php';
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$html="<table width='100%'><tr>
<td>Name</td>
<td>Age</td>
<td>Month</td>
<tr>
<td>Nahid</td>
<td>24</td>
<td>January</td>
</tr>
</tr></table>";


$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();