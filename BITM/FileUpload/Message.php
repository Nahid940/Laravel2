<?php


if(!isset($_SESSION)){
    session_start();
}

class message{
    
    public static function MyMessage($message=NULL){
        if(is_null($message)){
            $_message=self::getMessage();
            return $_message;
        }
        else{
            self::setMessage($message);
        }
    }
    
    
    public static function setMessage($message){
        $_SESSION['message']=$message;
    }
    
    public static function getMessage(){
        $_message=$_SESSION['message'];
        $_SESSION['message']='';
        return $_message;
    } 
}
?>