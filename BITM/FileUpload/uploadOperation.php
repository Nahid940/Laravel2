<?php
include_once('Message.php');
class Upload{
    public $username="root";
    public $password="";
    public $image="";
    public $name="";
    
    public $con="";
    
    
    public function __construct(){
        try{
            $this->con=new PDO('mysql:host=localhost;dbname=imageupload',$this->username,$this->password);
            $this->con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exp) {
            echo $exp->getMessage();
        }   
    }
    
    
    
    public function prepare1($data=array()){
        if(is_array($data) && array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        
        if(is_array($data) && array_key_exists('image',$data)){
            $this->image=$data['image'];
        }
        
        return $this;
    }
    
    
    public function insert()
    {
        
        if(!empty($this->name) && !empty($this->image)){
        
        $sql="insert into uploadimage(name,profilepic) values(:name,:image)";
        $q=$this->con->prepare($sql);
        $q->execute(array(':name'=>$this->name,':image'=>$this->image));
        }
        
        if($q){
            message::MyMessage("Data inserted");
            header("location:index.php");
        }else{
            message::MyMessage("Error");
            header('location:upload.php');
        }
        
    
    
    }
}

?>