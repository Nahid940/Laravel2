<?php

include('uploadOperation.php');
$upload=new Upload();

if(isset($_FILES)){
   
    $image_name=$_FILES['image']['name'];
    $image_type=$_FILES['image']['type'];
    $image_size=$_FILES['image']['size'];
    $tmp_name=$_FILES['image']['tmp_name'];
    
    $file_ext=strtolower(end(explode(".",$_FILES['image']['name'])));
    
    $error=array();
    $file_format=array('jpg','jpeg','png');
    
    if(in_array($file_ext,$file_format)===false){
        $error[]="File format not supported";
    }
    
    if($image_size>50000){
        $error[]="File size too large";
    }
    
    
    if(empty($error)==true){
        move_uploaded_file($tmp_name,"uploads/".$image_name);
        
    }else{
        print_r($error);
    }

     $upload->prepare1($_FILES)->insert();   
}

?>