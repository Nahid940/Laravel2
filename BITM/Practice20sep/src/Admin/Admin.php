<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/20/2017
 * Time: 10:31 PM
 */

namespace App\Admin;

use App\DB;

class Admin extends DB
{
    private $name;
    private $age;

    public  function set($data){
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
            $this->age=$data['age'];
        }
    }

    public function insertData(){
        $sql="insert into userinfo (name,age) value(:name,:age)";

        $stmt=$this->con->prepare($sql);
        $result=$stmt->execute(array(':name'=>$this->name, ':age'=>$this->age));
        if($result){
            session_start();
            $_SESSION['msg']="Data inserted";
            header('location:index.php');
        }
    }
}