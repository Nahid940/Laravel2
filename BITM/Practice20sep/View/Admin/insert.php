<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/20/2017
 * Time: 10:39 PM
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">WebSiteName</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Page 1</a></li>
                <li><a href="#">Page 2</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-12">

            <div class="col-md-4 pull-right">
                <form action="store.php" method="post">

                    <div class="form-group">
                        <label for="name"> Eneter name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="age"> Eneter age</label>
                        <input type="text" name="age" class="form-control">
                    </div>
                    <div class="form-group pull-right">

                        <button type="submit" class="btn btn-success">Insert</button>
                    </div>

                </form>
            </div>
        </div>

    </div>


</div>

</body>
</html>
