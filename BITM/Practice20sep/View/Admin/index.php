<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/20/2017
 * Time: 10:58 PM
 */
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    if(isset($_SESSION['msg']))

    echo $_SESSION['msg'];
    session_unset();
    ?>
</body>
</html>
