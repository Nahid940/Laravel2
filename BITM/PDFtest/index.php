<?php
    require_once 'fpdf.php';

    try{
        $con=new PDO('mysql:host=localhost;dbname=student',"root","");
    }catch (PDOException $exp){
        echo $exp->getMessage();
    }

class PDF extends FPDF
{
// Page header
    function Header()
    {
        // Logo
        $this->Image('Screenshot_7.jpg', 10, 6, 30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(90);
        // Title
        $this->Cell(100, 50, 'My test data',0, 1);
        // Line break
        $this->Ln(0);


        $this->Cell(40,5,'ID',1,0);
        $this->Cell(80,5,'Name',1,0);
        $this->Cell(70,5,'Grade',1,1);
       $this-> SetAuthor("Nahid", [true]);

    }

    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages('{pages}');
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
$pdf->SetDrawColor(50,50,100);

$sql="select * from studentdetails";
$stmt=$con->prepare($sql);
$stmt->execute();

foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data){
//    $pdf->SetDrawColor(0, 38, 51);
    $pdf->SetFillColor(0, 38, 51);
    $pdf->SetTextColor(0, 77, 26);

    $pdf->Cell(40,5,$data['Student_id'],1,0);
    $pdf->Cell(80,5,$data['Student_name'],1,0);
    $pdf->Cell(70,5,$data['Student_grade'],1,1);
}

$pdf->Output();
?>