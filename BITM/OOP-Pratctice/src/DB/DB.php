<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/17/2017
 * Time: 10:00 PM
 */
namespace App\DB;
class DB
{
    private static $pdo;
    public static $val=20;
    public static function myDb(){
        try{
            self::$pdo=new \PDO('mysql:host=localhost;dbname=info',"root","");
        }catch (\PDOException $exp){
            return $exp->getMessage();
        }
        return self::$pdo;

    }

    public static function myQuery($query){
        return self::myDb()->prepare($query);
    }

}