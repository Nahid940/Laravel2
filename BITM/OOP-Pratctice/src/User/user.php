<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/17/2017
 * Time: 10:14 PM
 */
namespace App\User;
use App\Acssssss\PersonInfo;
use App\DB\DB;


class user extends PersonInfo
{
    private $name;
    private $age;

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $gae
     */
    public function setGae($age)
    {
        $this->age = $age;
    }

    public function insertPersonInfo()
    {
        // TODO: Implement insertPersonInfo() method.
		if($this->name!='' || $this->age!=''){
		$sql="insert into userinfo (name,age) values(:name,:age)";
        $stmt=DB::myQuery($sql);
        $stmt->bindValue(':name',$this->name);
        $stmt->bindValue(':age',$this->age);
        $stmt->execute();
        return true;
		}else{
			return false;
		}
        
    }

    public function getUserInfo()
    {
        // TODO: Implement getUserInfo() method.

        $sql="select * from userinfo";
        $stmt=DB::myQuery($sql);
		
        $stmt->execute();
        return $stmt->fetchAll();
    }


}